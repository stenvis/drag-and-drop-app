const loader = () => {
  const link = document.createElement('link');
  link.rel = 'stylesheet';
  link.href = '/app/styles/main.css';
  link.async = false;
  link.type = 'text/css';
  document.head.appendChild(link);

  fetch('/app/html/frame.json')
     .then(frameJSON => frameJSON.json())
     .then(frame => {
        document.body.insertAdjacentHTML('beforeend', frame);
     });

  const script = document.createElement('script');
  script.src = '/app/fetch.js';
  script.async = false;
  script.type = 'module';
  document.head.appendChild(script);
};

document.addEventListener('DOMContentLoaded', loader);
