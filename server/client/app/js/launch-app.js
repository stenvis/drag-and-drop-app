import pageDone from '/app/js/etc/page-done.js';
import element from '/app/js/etc/elements.js';
import container from '/app/js/drag-and-drop/drag-and-drop.js';

function initApp() {
  pageDone.init();
  element.init();
  container.init();
};

export default initApp;
