import element from '/app/js/etc/elements.js';

const pageDone = {
   init() {
      ready();
      removePreloader();
   },
};

function ready() {
   element.page.classList.add('done');
};

function removePreloader() {
   element.preloader.remove();
   document.head.children[2].remove();
   document.head.children[2].remove();
};

export default pageDone;
