const element = {
   page: document.getElementById('page'),
   preloader: document.getElementById('preloader'),
   middle: document.getElementById('middle'),
   imgReceiver: document.getElementById('image-receiver'),
   init() { initElements() },
};

function initElements() {
  element.selectFile = document.getElementById('file-upload');
  element.infoLocation = document.getElementById('info-location');
  element.sendFile = document.getElementById('send-file');
  element.dropBox = document.getElementById('drop-box');
};

export default element;
