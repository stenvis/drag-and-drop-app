import element from '/app/js/etc/elements.js';
import evList from '/app/js/drag-and-drop/ev-list.js';

function setHighlight() {
  const 
    dB = element.dropBox,
    list1 = [evList[0], evList[1]],
    list2 = [evList[2], evList[3]];

  list1.forEach(ev => { dB.addEventListener(ev, isActive, false); });
  list2.forEach(ev => { dB.addEventListener(ev, notActive, false); });

  function isActive(e) { dB.classList.add('active'); };
  function notActive(e) { dB.classList.remove('active'); };
};

export default setHighlight;
