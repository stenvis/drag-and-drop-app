import element from '/app/js/etc/elements.js';

function check(type) {
  const list = [ 
    'image/jpg',
    'image/jpeg',
    'image/png',
    'image/gif',
    'image/pdf',
  ];

  if (list.includes(type)) return true;
  else return false;
};

function uploader(file) {
  if (check(file.type)) { 
    loadMess();
    begin(file);
  } else notSupMess();
};

function begin(file) {
  const 
    url = '/image-receiver',
    formData = new FormData().append('file', file, 'file.png');

  fetch(url, {
    method: 'POST',
    body: formData,
  })
  .then(done => done.text())
  .then(text => {
    if (text === 'ok') {
      const reader = new FileReader();
      reader.readAsDataURL(file); 
      reader.onloadend = function() {
        const img = new Image(150, 150);
        img.src = reader.result;
        element.imgReceiver.appendChild(img);
        resMess();
      };
    };
  });
};

function loadMess() {
  element.infoLocation.innerHTML = "Loading...";
};

function notSupMess() {
  element.infoLocation.innerHTML = "This format isn't supported";
};

function resMess() {
  element.infoLocation.innerHTML = "<span>&#8249;</span>Drag Your Image<span>&#8250;</span>";
};

export default uploader;
