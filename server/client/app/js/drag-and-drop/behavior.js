import element from '/app/js/etc/elements.js';
import evList from '/app/js/drag-and-drop/ev-list.js';
import uploader from '/app/js/drag-and-drop/uploader.js';

function preventDefaults(ev) {
  ev.preventDefault();
  ev.stopPropagation();
};

function setBehavior() {
  evList.forEach(ev => { element.page.addEventListener(ev, preventDefaults, false); });
  dropBoxHandler();
  dragAndDropHandler();
};

function dropBoxHandler() {
  const 
    slf = element.selectFile,
    sf = element.sendFile;

  const setName = () => {
    element.infoLocation.innerHTML = slf.files.length == 1 ?
      slf.files[0].name :
      slf.files[0].name + ' ...';
  };

  const send = ev => {
    preventDefaults(ev);
    const files = slf.files;
    [...files].forEach(uploader);
  };

  slf.addEventListener('change', setName);
  sf.addEventListener('click', send);
};

function dragAndDropHandler() {
  element.dropBox.addEventListener('drop', handleDrop, false)

  function handleDrop(e) {
    const files = e.dataTransfer.files;
    [...files].forEach(uploader);
  }
};

export default setBehavior;
