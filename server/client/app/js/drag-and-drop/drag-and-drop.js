import element from '/app/js/etc/elements.js';
import setHighlight from '/app/js/drag-and-drop/highlight.js';
import setBehavior from '/app/js/drag-and-drop/behavior.js';

const container = {
  init() { 
    setBehavior();
    setHighlight();
  },
};


export default container;
