import element from '/app/js/etc/elements.js';
import initApp from '/app/js/launch-app.js';

{
  fetch('/app/html/container.json')
    .then(containerJSON => containerJSON.json())
    .then(container => {
      element.middle.insertAdjacentHTML('beforeend', container);
      initApp();
    });
}
