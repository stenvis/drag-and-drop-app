#!/usr/bin/node

const fs = require('fs');

const frame = JSON.stringify(
`
<div id="drop-box">
  <p id="info-location"><span>&#8249;</span>Drag Your Image<span>&#8250;</span></p>
  <form class='form_input-file'>
    <label class='label_input-file' for="file-upload"><i>Choose images to upload (jpg, jpeg, png, gif, pdf)</i></label> 
    <label for="file-upload" class="custom-file-upload">
        <span>| Select file |</span>
    </label>
    <input id="file-upload" type="file"/>
    <input id="send-file" type="submit">
  </form>
</div>
`
);

fs.writeFileSync('./container.json', frame);
