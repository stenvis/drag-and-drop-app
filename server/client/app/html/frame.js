#!/usr/bin/node

const fs = require('fs');

const frame = JSON.stringify(
`
<div id="page">
  <header>
    <h1>Test App</h1>
  </header>
  <section id="middle">
    <div id="image-receiver">
    </div>
  </section>
  <footer>
    <p>by Stenvis</p>
  </footer>
</div>
`
);

fs.writeFileSync('./frame.json', frame);
